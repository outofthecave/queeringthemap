from django.apps import AppConfig


class PinprickConfig(AppConfig):
    name = 'pinprick'
